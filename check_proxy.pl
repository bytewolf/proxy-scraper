#!/usr/bin/env perl
# hui
# check_proxy.pl

use strict;
use AnyEvent::HTTP;

$| = 1;
my $MAX_PROC = 1000;
my $timeout = 10;

$AnyEvent::HTTP::MAX_RECURSE = 0;
$AnyEvent::HTTP::MAX_PER_HOST = $MAX_PROC;

my $target = "http://www.google.co.jp";
my $start = time;

my $cv = AnyEvent->condvar;
_check(*STDIN);
$cv->recv;

my $end = time;
print "time: ".($end - $start)."s\n";

sub _check {
    my $fh = shift;
    while($AnyEvent::HTTP::ACTIVE < $MAX_PROC) {
        my $proxy = <$fh>;
        defined $proxy or last;
        $proxy =~ s/\s//g;
        http_request(
            GET => $target,
            headers => { "user-agent" => "Mozilla/5.0" },
            timeout => $timeout,
            proxy => [split(/:/, $proxy)],
            on_header => sub {
                if ($_[0]{'server'} eq "gws") {
                    print "$proxy ok!\n";
                } else {
                    print "$proxy failed!\n";
                }
                return 0;
            },
            sub { _check($fh) },
        );
    }
    $cv->send if ($AnyEvent::HTTP::ACTIVE == 0 && eof($fh));
}

