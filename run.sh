#!/bin/bash
# run.sh

PERL=$(which perl)
SCRAPER=scrap_proxy.pl
CHECKER=check_proxy.pl
SCRAP_RESULT=tmp/,scrap_result
CHECK_RESULT=tmp/,check_result
PROXYLIST=data/,proxylist

echo "Scrapping proxy search result from Google..."
$PERL $SCRAPER >  $SCRAP_RESULT

echo "Checking proxy connectivity..."
grep : $SCRAP_RESULT | $PERL $CHECKER | tee $CHECK_RESULT

grep ok $CHECK_RESULT | awk '{print $1}' >> $PROXYLIST

echo -e "All done!\nAvailable proxies are listed in file $PROXYLIST.\n"
